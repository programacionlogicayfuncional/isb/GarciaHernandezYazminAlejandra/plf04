(ns plf04.core)

;;stringE
(defn string-e-1
  [x]
  (letfn [(g [xs]
            (cond
              (empty? xs) 0
              (= (first xs) \e) (+ 1 (g (rest xs)))
              :else (+ 0 (g (rest xs)))))
          (f [x]
            (<= 1 (g x) 3))]
    (f x)))

(string-e-1 "Hello")
(string-e-1 "Heelle")
(string-e-1 "Heelele")
(string-e-1 "Hll")
(string-e-1 "e")
(string-e-1 "")

(defn string-e-2
  [x]
  (letfn [(h [acc]
            (<= 1 acc 3))
          (g [xs acc]
            (if (= (first xs) \e) (inc acc) acc))
          (f [xs acc]
            (if (empty? xs)
              (h acc)
              (f (rest xs) (g xs acc))))]
    (f x 0)))

(string-e-2 "Hello")
(string-e-2 "Heelle")
(string-e-2 "Heelele")
(string-e-2 "Hll")
(string-e-2 "e")
(string-e-2 "")



;;stringTimes
(defn string-times-1
  [x n]
  (letfn [(f [x n]
            (if (zero? n)
              ""
              (str x (f x (dec n)))))]
    (f x n)))

(string-times-1 "Hi" 2)
(string-times-1 "Hi" 3)
(string-times-1 "Hi" 1)
(string-times-1 "Hi" 0)
(string-times-1 "Hi" 5)
(string-times-1 "Oh Boy!" 2)
(string-times-1 "x" 4)
(string-times-1 "" 4)
(string-times-1 "code" 2)
(string-times-1 "code" 3)

(defn string-times-2
  [x n]
  (letfn [(g [x acc]
            (str acc x))
          (f [x n acc]
            (if (zero? n)
              acc
              (f x (dec n) (g x acc))))]
    (f x n "")))

(string-times-2 "Hi" 2)
(string-times-2 "Hi" 3)
(string-times-2 "Hi" 1)
(string-times-2 "Hi" 0)
(string-times-2 "Hi" 5)
(string-times-2 "Oh Boy!" 2)
(string-times-2 "x" 4)
(string-times-2 "" 4)
(string-times-2 "code" 2)
(string-times-2 "code" 3)



;;frontTimes
(defn front-times-1
  [x n]
  (letfn [(h [x]
            (if (empty? x)
              ""
              (str (first x) (h (rest x)))))
          (g [x]
            (h (take 3 x)))
          (f [x n]
            (if (zero? n)
              ""
              (str (g x) (f x (dec n)))))]
    (f x n)))

(front-times-1 "Chocolate" 2)
(front-times-1 "Chocolate" 3)
(front-times-1 "Abc" 3)
(front-times-1 "Ab" 4)
(front-times-1 "A" 4)
(front-times-1 "" 4)
(front-times-1 "Abc" 0)

(defn front-times-2
  [x n]
  (letfn [(h [x]
            (if (empty? x)
              ""
              (str (first x) (h (rest x)))))
          (g [x acc]
            (str acc (h (take 3 x))))
          (f [x n acc]
            (if (zero? n)
              acc
              (f x (dec n) (g x acc))))]
    (f x n "")))

(front-times-2 "Chocolate" 2)
(front-times-2 "Chocolate" 3)
(front-times-2 "Abc" 3)
(front-times-2 "Ab" 4)
(front-times-2 "A" 4)
(front-times-2 "" 4)
(front-times-2 "Abc" 0)



;;countXX
(defn count-xx-1
  [x]
  (letfn [(g [x]
            (if (= (concat (take 2 x)) [\x \x]) 1 0))
          (f [x]
            (if (empty? x)
              0
              (+ (g x) (f (rest x)))))]
    (f x)))

(count-xx-1 "abcxx")
(count-xx-1 "xxx")
(count-xx-1 "xxxx")
(count-xx-1 "abc")
(count-xx-1 "Hello there")
(count-xx-1 "Hexxo thxxe")
(count-xx-1 "")
(count-xx-1 "Kittens")
(count-xx-1 "Kittensxxx")

(defn count-xx-2
  [x]
  (letfn [(h [x]
            (if (= (concat (take 2 x)) [\x \x]) 1 0))
          (g [x acc]
            (+ acc (h x)))
          (f [x acc]
            (if (empty? x)
              acc
              (f (rest x) (g x acc))))]
    (f x 0)))

(count-xx-2 "abcxx")
(count-xx-2 "xxx")
(count-xx-2 "xxxx")
(count-xx-2 "abc")
(count-xx-2 "Hello there")
(count-xx-2 "Hexxo thxxe")
(count-xx-2 "")
(count-xx-2 "Kittens")
(count-xx-2 "Kittensxxx")



;;stringSplosion
(defn string-splosion-1
  [x]
  (letfn [(h [x]
            (if (empty? x)
              ""
              (str (first x) (h (rest x)))))
          (g [x n]
            (h (take n x)))
          (f [x n]
            (if (zero? n)
              ""
              (str (f x (dec n)) (g x n))))]
    (f x (count x))))

(string-splosion-1 "Code")
(string-splosion-1 "abc")
(string-splosion-1 "ab")
(string-splosion-1 "x")
(string-splosion-1 "fade")
(string-splosion-1 "There")
(string-splosion-1 "Kitten")
(string-splosion-1 "Bye")
(string-splosion-1 "Good")
(string-splosion-1 "Bad")

(defn string-splosion-2
  [x]
  (letfn [(h [x]
            (if (empty? x)
              ""
              (str (first x) (h (rest x)))))
          (g [x n acc]
            (str acc (h (take n x))))
          (f [x n acc]
            (if (> n (count x))
              acc
              (f x (inc n) (g x n acc))))]
    (f x 0 "")))

(string-splosion-2 "Code")
(string-splosion-2 "abc")
(string-splosion-2 "ab")
(string-splosion-2 "x")
(string-splosion-2 "fade")
(string-splosion-2 "There")
(string-splosion-2 "Kitten")
(string-splosion-2 "Bye")
(string-splosion-2 "Good")
(string-splosion-2 "Bad")



;;array123
(defn array-123-1
  [xs]
  (letfn [(g [xs]
            (if (= (take 3 xs) [1 2 3]) true false))
          (f [xs]
            (if (empty? xs)
              false
              (or (f (rest xs)) (g xs))))]
    (f xs)))

(array-123-1 [1 1 2 3 1])
(array-123-1 [1 1 2 4 1])
(array-123-1 [1 1 2 1 2 3])
(array-123-1 [1 1 2 1 2 1])
(array-123-1 [1 2 3 1 2 3])
(array-123-1 [1 2 3])
(array-123-1 [1 1 1])
(array-123-1 [1 2])
(array-123-1 [1])
(array-123-1 [])

(defn array-123-2
  [xs]
  (letfn [(h [xs]
            (= (take 3 xs) [1 2 3]))
          (g [xs acc]
            (or acc (h xs)))
          (f [xs acc]
            (if (empty? xs)
              acc
              (f (rest xs) (g xs acc))))]
    (f xs false)))

(array-123-2 [1 1 2 3 1])
(array-123-2 [1 1 2 4 1])
(array-123-2 [1 1 2 1 2 3])
(array-123-2 [1 1 2 1 2 1])
(array-123-2 [1 2 3 1 2 3])
(array-123-2 [1 2 3])
(array-123-2 [1 1 1])
(array-123-2 [1 2])
(array-123-2 [1])
(array-123-2 [])



;;stringX
(defn string-x-1
  [x]
  (letfn [(g [x n]
            (if (and (= (get x n) \x) (< 0 n (- (count x) 1)))
              ""
              (get x n)))
          (f [x n]
            (if (< n 0)
              ""
              (str (f x (dec n)) (g x n))))]
    (f x (count x))))

(string-x-1 "abxxxcd")
(string-x-1 "xabxxxcdx")
(string-x-1 "xKittenx")
(string-x-1 "Hello")
(string-x-1 "xx")
(string-x-1 "x")
(string-x-1 "")

(defn string-x-2
  [x]
  (letfn [(h [x n]
            (if (and (= (get x n) \x) (< 0 n (- (count x) 1)))
              ""
              (get x n)))
          (g [x n acc]
            (str acc (h x n)))
          (f [x n acc]
            (if (= n (count x))
              acc
              (f x (inc n) (g x n acc))))]
    (f x 0 "")))

(string-x-2 "xxHxix")
(string-x-2 "abxxxcd")
(string-x-2 "xabxxxcdx")
(string-x-2 "xKittenx")
(string-x-2 "Hello")
(string-x-2 "xx")
(string-x-2 "x")
(string-x-2 "")



;;altPairs
(defn alt-pairs-1
  [x]
  (letfn [(h [n]
            (or (= n 0) (= n 1) (= n 4) (= n 5) (= n 8) (= n 9)))
          (g [x n]
            (if (h n) (get x n) ""))
          (f [x n]
            (if (< n 0)
              ""
              (str (f x (dec n)) (g x n))))]
    (f x (count x))))

(alt-pairs-1 "kitten")
(alt-pairs-1 "Chocolate")
(alt-pairs-1 "CodingHorror")
(alt-pairs-1 "yak")
(alt-pairs-1 "ya")
(alt-pairs-1 "y")
(alt-pairs-1 "")
(alt-pairs-1 "ThisThatTheOther")

(defn alt-pairs-2
  [x]
  (letfn [(i [n]
            (or (= n 0) (= n 1) (= n 4) (= n 5) (= n 8) (= n 9)))
          (h [x n]
            (if (i n) (get x n) ""))
          (g [x n acc]
            (str acc (h x n)))
          (f [x n acc]
            (if (= n (count x))
              acc
              (f x (inc n) (g x n acc))))]
    (f x 0 "")))

(alt-pairs-2 "kitten")
(alt-pairs-2 "Chocolate")
(alt-pairs-2 "CodingHorror")
(alt-pairs-2 "yak")
(alt-pairs-2 "ya")
(alt-pairs-2 "y")
(alt-pairs-2 "")
(alt-pairs-2 "ThisThatTheOther")



;;stringYark
(defn string-yark-1
  [x]
  (letfn [(g [x]
            (and (= (first x) \y) (= (last x) \k)))
          (f [x]
            (cond
              (empty? x) ""
              (g (take 3 x)) (str "" (f (rest (rest (rest x)))))
              :else (str (first x) (f (rest x)))))]
    (f x)))

(string-yark-1 "yakpak")
(string-yark-1 "pakyak")
(string-yark-1 "yak123ya")
(string-yark-1 "yak")
(string-yark-1 "yakxxxyak")
(string-yark-1 "HiyakHi")
(string-yark-1 "xxxyakyyyakzzz")

(defn string-yark-2
  [x]
  (letfn [(h [x]
            (and (= (first x) \y) (= (last x) \k)))
          (g [acc x]
            (str acc x))
          (f [x acc]
            (cond
              (empty? x) acc
              (h (take 3 x)) (f (rest (rest (rest x))) (g acc ""))
              :else (f (rest x) (g acc (first x)))))]
    (f x "")))

(string-yark-2 "yakpak")
(string-yark-2 "pakyak")
(string-yark-2 "yak123ya")
(string-yark-2 "yak")
(string-yark-2 "yakxxxyak")
(string-yark-2 "HiyakHi")
(string-yark-2 "xxxyakyyyakzzz")



;;has271
(defn has-271-1
  [x]
  (letfn [(i [x]
            (= x [2 7 1]))
          (h [x y]
            (<= (- x 2) y (+ x 2)))
          (g [x]
            (and (= (first (rest x)) (+ 5 (first x))) (h (- (first x) 1) (last x))))
          (f [x]
            (cond
              (empty? x) false
              (or (i (take 3 x)) (g (take 3 x))) (or true (f (rest x)))
              :else (or false (f (rest x)))))]
    (f x)))

(has-271-1 [1 2 7 1])
(has-271-1 [1 2 8 1])
(has-271-1 [2 7 1])
(has-271-1 [3 8 2])
(has-271-1 [2 7 3])
(has-271-1 [2 7 4])
(has-271-1 [2 7 -1])
(has-271-1 [2 7 -2])
(has-271-1 [4 5 3 8 0])
(has-271-1 [2 7 5 10 4])
(has-271-1 [2 7 -2 4 9 3])
(has-271-1 [2 7 5 10 1])
(has-271-1 [2 7 -2 4 10 2])
(has-271-1 [1 1 4 9 0])
(has-271-1 [1 1 4 9 4 9 2])

(defn has-271-2
  [x]
  (letfn [(j [x]
            (= x [2 7 1]))
          (i [x y]
            (<= (- x 2) y (+ x 2)))
          (h [x]
            (and (= (first (rest x)) (+ 5 (first x))) (i (- (first x) 1) (last x))))
          (g [x acc]
            (or acc (or (j (take 3 x)) (h (take 3 x)))))
          (f [x acc]
            (if (empty? x)
              acc
              (f (rest x) (g x acc))))]
    (f x false)))

(has-271-2 [1 2 7 1])
(has-271-2 [1 2 8 1])
(has-271-2 [2 7 1])
(has-271-2 [3 8 2])
(has-271-2 [2 7 3])
(has-271-2 [2 7 4])
(has-271-2 [2 7 -1])
(has-271-2 [2 7 -2])
(has-271-2 [4 5 3 8 0])
(has-271-2 [2 7 5 10 4])
(has-271-2 [2 7 -2 4 9 3])
(has-271-2 [2 7 5 10 1])
(has-271-2 [2 7 -2 4 10 2])
(has-271-2 [1 1 4 9 0])
(has-271-2 [1 1 4 9 4 9 2])